﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TodoListApp_ASPNetCoreMVC.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        public string PasswordHash { get; set; }

        [Required]
        public string PasswordSalt { get; set; }

        // New property for receiving password input from the view
        [NotMapped] // NotMapped attribute ensures this property is not mapped to database
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
