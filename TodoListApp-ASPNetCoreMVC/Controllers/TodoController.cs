﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TodoListApp_ASPNetCoreMVC.Data;
using TodoListApp_ASPNetCoreMVC.Models;
using System.Linq;

namespace TodoListApp_ASPNetCoreMVC.Controllers
{
    [Authorize]
    public class TodoController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TodoController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var username = User.Identity.Name;
            var user = _context.Users.SingleOrDefault(u => u.Username == username);
            var todos = _context.TodoItems.Where(t => t.UserId == user.Id).ToList();
            return View(todos);
        }

        [HttpPost]
        public IActionResult Add(string task)
        {
            var username = User.Identity.Name;
            var user = _context.Users.SingleOrDefault(u => u.Username == username);
            var todo = new TodoItem { Task = task, IsCompleted = false, UserId = user.Id };
            _context.TodoItems.Add(todo);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult UpdateStatus(int id, bool isCompleted)
        {
            var todo = _context.TodoItems.Find(id);
            if (todo != null)
            {
                todo.IsCompleted = isCompleted;
                _context.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            var todo = _context.TodoItems.Find(id);
            if (todo == null)
            {
                return NotFound();
            }
            return View(todo);
        }

        [HttpPost]
        public IActionResult Edit(TodoItem todoItem)
        {
            if (ModelState.IsValid)
            {
                var existingTodoItem = _context.TodoItems.Find(todoItem.Id);

                if (existingTodoItem == null) 
                {
                    return NotFound();
                }

                // Only update the necessary fields
                existingTodoItem.Task = todoItem.Task;
                existingTodoItem.IsCompleted = todoItem.IsCompleted;

                _context.TodoItems.Update(existingTodoItem);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(todoItem);
        }

        public IActionResult Delete(int id)
        {
            var todo = _context.TodoItems.Find(id);
            if (todo == null)
            {
                return NotFound();
            }
            return View(todo);
        }

        [HttpPost, ActionName("Delete")]
        public IActionResult DeleteConfirmed(int id)
        {
            var todo = _context.TodoItems.Find(id);
            if (todo != null)
            {
                _context.TodoItems.Remove(todo);
                _context.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
}
