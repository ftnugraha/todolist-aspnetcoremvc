﻿using Microsoft.EntityFrameworkCore;
using TodoListApp_ASPNetCoreMVC.Models;

namespace TodoListApp_ASPNetCoreMVC.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base (options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<TodoItem> TodoItems { get; set; }

    }
}
