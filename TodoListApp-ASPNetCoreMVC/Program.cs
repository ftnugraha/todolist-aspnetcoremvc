using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.EntityFrameworkCore;
using TodoListApp_ASPNetCoreMVC.Data;

namespace TodoListApp_ASPNetCoreMVC
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            // Konfigurasi database
            builder.Services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

            // Tambahkan layanan autentikasi menggunakan cookies
            builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.LoginPath = "/Account/Login";
                    options.AccessDeniedPath = "/Account/Login";
                    options.ExpireTimeSpan = TimeSpan.FromMinutes(30); // Mengatur waktu kedaluwarsa cookie menjadi 30 menit
                    options.SlidingExpiration = true; // Memperbarui waktu kedaluwarsa cookie jika pengguna aktif
                    options.Cookie.IsEssential = true; // Mark the cookie as essential for GDPR compliance
                    options.Cookie.HttpOnly = true; // Cookie tidak bisa diakses melalui JavaScript
                    options.Cookie.SecurePolicy = CookieSecurePolicy.Always; // Hanya kirim cookie melalui HTTPS
                });

            // Tambahkan layanan autorisasi
            builder.Services.AddAuthorization();

            // Tambahkan layanan MVC dan session
            builder.Services.AddControllersWithViews();
            builder.Services.AddSession();
            builder.Services.AddDistributedMemoryCache();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            // Middleware session
            app.UseSession();

            // Middleware autentikasi dan autorisasi
            app.UseAuthentication();
            app.UseAuthorization();

            app.MapControllerRoute(
                name: "default",
                pattern: "{controller=Home}/{action=Index}/{id?}");

            app.Run();
        }
    }
}
